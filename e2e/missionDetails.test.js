const config = require("../config/config.js");

describe("mission details page", () => {
  beforeAll(async () => {
    await page.goto(`http://localhost:${config.localhostPort}/launch/5fe3b107b3467846b324216b`, {
      waitUntil: "networkidle2",
    });
  });

  it("should display launch name", async () => {
    await expect(page).toMatch("DART");
  });
});
