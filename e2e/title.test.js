const config = require("../config/config.js");

describe("Expect page to have specified title", () => {
  beforeAll(async () => {
    await page.goto(`http://localhost:${config.localhostPort}`, { waitUntil: "domcontentloaded" });
  });
  test("page title", async () => {
    const title = await page.title();
    expect(title).toBe("spaceX-launches");
  });
});
