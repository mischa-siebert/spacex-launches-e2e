const config = require("../config/config.js");

describe("unmanned missions table", () => {
  beforeAll(async () => {
    await page.goto(`http://localhost:${config.localhostPort}`, { waitUntil: "networkidle2" });
  });

  it("should display unmanned launch name", async () => {
    const text = await Promise.all([
      page.waitForTimeout(500),
      page.click("#unmanned"),
    ]).then(() => page.evaluate(() => document.body.innerHTML));

    expect(text).toContain("Turksat");
    expect(text).toContain("USSF-52");
  });
});
