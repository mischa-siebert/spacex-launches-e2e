const config = require("../config/config.js");

describe("Expect page to contain specified tab texts", () => {
  beforeAll(async () => {
    await page.goto(`http://localhost:${config.localhostPort}`, {
      waitUntil: "domcontentloaded",
    });
  });

  test("tab texts", async () => {
    await expect(page).toMatch("Manned missions");
    await expect(page).toMatch("Unmanned missions");
  });
});
