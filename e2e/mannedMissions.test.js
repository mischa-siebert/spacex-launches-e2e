const config = require("../config/config.js");

describe("spacex-launches main page", () => {
  beforeAll(async () => {
    await page.goto(`http://localhost:${config.localhostPort}`, { waitUntil: "networkidle2" });
  });

  it('should display "Crew-1", "Crew-2", "Crew-3" texts on page', async () => {
    await expect(page).toMatch("Crew-1");
    await expect(page).toMatch("Crew-2");
    await expect(page).toMatch("Crew-3");
  });
});
