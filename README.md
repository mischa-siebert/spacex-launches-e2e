# spacex-launches-e2e

This repository contains end-to-end tests for the spacex-launches app found [here](https://bitbucket.org/mischa-siebert/spacex-launches/src/master/).

E2e tests were implemented using the puppeteer library and jest.

## Run e2e tests

### NOTE
E2e tests are currently configured to run against the docker container on port `3001`. If you want to run the app locally and run the e2e tests against it, in `config/config.js` set `localhostPort = 3000`.

### Instructions

1. Run the app preferably on Docker.
2. Run `npm install` in this project's root directory.
3. When the app is available on `localhost:3001` RUN `npm run test` in this project's root directory.
